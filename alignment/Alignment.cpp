#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include "Alignment.hpp"

Alignment::Alignment(const std::string &seq_v, const std::string &seq_h)
{
    scoreBoard.clear();
    seqVert = seq_v;
    seqHori = seq_h;
    seqVert_length = seqVert.length();
    seqHori_length = seqHori.length();
    scoreBoard.resize((seqVert_length+1) * (seqHori_length+1), 0); // matrix implementation with one vector

}

void Alignment::compute (const int  match,
                         const int  mismatch,
                         const int  gap,
                         const bool local_align)
{
    traceback.clear();
    localign = local_align;
    if (localign == 0)
    {   // initialisation for the first column and row
        for (int i = 0; i < seqHori_length+1; i++)
            scoreBoard[i] = i * gap;
        for (int i = 0; i < seqVert_length+1; i++)
            scoreBoard[i*(seqHori_length+1)] = i * gap;

        // iterator within a column
        for (int i = 1; i < seqVert_length+1; ++i)
        {
            // iterator within a row
            for (int j = 1; j < seqHori_length+1; ++j )
            {
                // S(xi,yj)
                int Sij = mismatch;
                if (seqVert[i-1] == seqHori[j-1]) Sij = match;

                // cell containing the score for comparison of seqVert[i-1] and seqHori[j-1]
                scoreBoard [i * (seqHori_length+1) + j] =
                        //F(i-1, j) + d
                        std::max( (scoreBoard[(i-1) * (seqHori_length+1) +  j   ] + gap),
                        //F(i, j-1) + d
                        std::max( (scoreBoard[ i    * (seqHori_length+1) + (j-1)] + gap),
                                 //F(i-j,j-1) + S(xi,yj)
                                  (scoreBoard[(i-1) * (seqHori_length+1) + (j-1)] + Sij) ) );
            }

        }

        //Visual interface
        /*std::cout << "      ";
        for (char c:seqHori)
            std::cout<< c << "  "; //outs the horizontal sequence
        std::cout << std::endl;

        int sqVletter = 0;
        for (uint g = 0; g < scoreBoard.size(); g += (seqHori_length+1))
        {
            if (g == 0) std::cout << "   ";
            //outs the vertical sequence
            else
            {
                std::cout << seqVert[sqVletter] << "  ";
                sqVletter++;
            }
            //outs the matrix itself
            for (uint i = g; i < g + (seqHori_length+1); i++)
            {
                if (scoreBoard[i] < 0 || scoreBoard[i] >9) std::cout << scoreBoard[i]<< " ";
                else std::cout << scoreBoard[i]<< "  ";
            }
            std::cout << std::endl;
        }*/

        // index of the last cell of the scoring matrix
        int indexOfLC = std::distance (scoreBoard.begin(), scoreBoard.end());
        indexOfLC--;

        score_ = scoreBoard[indexOfLC];

        //Traceback
        while (indexOfLC%(seqHori_length+1) != 0 && indexOfLC > seqHori_length)
        {
            // the second condition is necessary for the cases,
            // where there might be coincidental match between a "match" and a "gap"
            // the other check don't need it due to the established priority list
            if      (scoreBoard[indexOfLC] == scoreBoard[indexOfLC - 1 - (seqHori_length + 1)] + match &&
                     seqVert[indexOfLC/(seqHori_length+1)-1] == seqHori[indexOfLC%(seqHori_length+1)-1])
            {
                indexOfLC -= (1 + (seqHori_length + 1));
                //std::cout << scoreBoard[indexOfLC] << " " << indexOfLC << std::endl;
                traceback.insert(traceback.begin(), Traceback::DIAGONAL);
            }
            // although the "mismatch" corresponds to the cursor moving diagonally,
            // we give it a "NONE" as to be able to differentiate between "mismatch" and a "match"
            // which is essential for creating the "gaps" string
            else if (scoreBoard[indexOfLC] == scoreBoard[indexOfLC - 1 - (seqHori_length + 1)] + mismatch)
            {
                indexOfLC -= (1 + (seqHori_length + 1));
                //std::cout << scoreBoard[indexOfLC] << " " << indexOfLC << std::endl;
                traceback.insert(traceback.begin(), Traceback::NONE);
            }

            else if (scoreBoard[indexOfLC] == scoreBoard[indexOfLC - (seqHori_length + 1)] +  gap)
            {
                indexOfLC -= (seqHori_length+1);
                //std::cout << scoreBoard[indexOfLC] << " " << indexOfLC << std::endl;
                traceback.insert(traceback.begin(), Traceback::VERTICAL);
            }

            else// if (scoreBoard[indexOfLC] == scoreBoard[indexOfLC - 1] - gap)
            {
                indexOfLC--;
                //std::cout << scoreBoard[indexOfLC] << " " << indexOfLC << std::endl;
                traceback.insert(traceback.begin(), Traceback::HORIZONTAL);
            }


        }
        // in case we arive at the borders of the matrix
        if (indexOfLC > seqHori_length) // this one is the first column
            while (indexOfLC > 0)
            {
                indexOfLC -= (seqHori_length+1);
                //std::cout << scoreBoard[indexOfLC] << " " << indexOfLC << std::endl;
                traceback.insert (traceback.begin(), Traceback::VERTICAL);
            }
        else                 // this one is the first row
            while (indexOfLC > 0)
            {
                indexOfLC--;
                //std::cout << scoreBoard[indexOfLC] << " " << indexOfLC << std::endl;
                traceback.insert (traceback.begin(), Traceback::HORIZONTAL);
            }

        itVert = 0;
        itHori = 0;

        didComputerun = true;
        return;
    }
    else
    {
        // unlike the global alignment, local doesn't need an initialisation for the first column and first row
        // due to the fact, that we initialised the scoring matrix filled with 0 in every cell we can simply
        // start calculation scores for the cells starting with (1,1) cell

        // iterator within a column
        for (int i = 1; i < seqVert_length + 1; ++i)
        {
            // iterator within a row
            for (int j = 1; j < seqHori_length + 1; ++j)
            {
                // S(xi,yj)
                int Sij = mismatch;
                if (seqVert[i - 1] == seqHori[j - 1]) Sij = match;

                // cell containing the score for comparison of seqVert[i-1] and seqHori[j-1]
                scoreBoard[i * (seqHori_length + 1) + j] =
                        // local alignment allows for start of the alignment at any point of (i,j) pair
                        std::max(0,
                                //F(i-1, j) + d
                        std::max((scoreBoard[(i - 1) * (seqHori_length + 1) + j] + gap),
                                //F(i, j-1) + d
                        std::max((scoreBoard[i * (seqHori_length + 1) + (j - 1)] + gap),
                                //F(i-j,j-1) + S(xi,yj)
                                 (scoreBoard[(i - 1) * (seqHori_length + 1) + (j - 1)] + Sij))));
            }
        }
        //Visual interface
        /*std::cout << "      ";
        for (char c:seqHori)
            std::cout<< c << "  "; //outs the horizontal sequence
        std::cout << std::endl;

        int sqVletter = 0;
        for (uint g = 0; g < scoreBoard.size(); g += (seqHori_length+1))
        {
            if (g == 0) std::cout << "   ";
            //outs the vertical sequence
            else
            {
                std::cout << seqVert[sqVletter] << "  ";
                sqVletter++;
            }
            //outs the matrix itself
            for (uint i = g; i < g + (seqHori_length+1); i++)
            {
                if (scoreBoard[i] < 0 || scoreBoard[i] >9) std::cout << scoreBoard[i]<< " ";
                else std::cout << scoreBoard[i]<< "  ";
            }
            std::cout << std::endl;
        }*/

        // index of the highest score cell
        int indexOfHS = std::distance (scoreBoard.begin(), std::max_element(scoreBoard.begin(),scoreBoard.end()));
        // std::cout << scoreBoard[indexOfHS] << std::endl;
        score_ = scoreBoard[indexOfHS];
        //Traceback
        while (scoreBoard[indexOfHS] != 0)
        {
            // the second condition is necessary for the cases,
            // where there might be coincidental match between a "match" and a "gap"
            // the other check don't need it due to the established priority list
            if      (scoreBoard[indexOfHS] == scoreBoard[indexOfHS - 1 - (seqHori_length + 1)] + match &&
                     seqVert[indexOfHS/(seqHori_length+1)-1] == seqHori[indexOfHS%(seqHori_length+1)-1])
            {
                indexOfHS -= (1 + (seqHori_length + 1));
                //std::cout << scoreBoard[indexOfHS] << " " << indexOfHS << std::endl;
                traceback.push_back(Traceback::DIAGONAL);
            }
                // although the "mismatch" corresponds to the cursor moving diagonally,
                // we give it a "NONE" as to be able to differentiate between "mismatch" and a "match"
                // which is essential for creating the "gaps" string
            else if (scoreBoard[indexOfHS] == scoreBoard[indexOfHS - 1 - (seqHori_length + 1)] + mismatch)
            {
                indexOfHS -= (1 + (seqHori_length + 1));
                //std::cout << scoreBoard[indexOfHS] << " " << indexOfHS << std::endl;
                traceback.push_back(Traceback::NONE);
            }

            else if (scoreBoard[indexOfHS] == scoreBoard[indexOfHS - (seqHori_length + 1)] +  gap)
            {
                indexOfHS -= (seqHori_length+1);
                //std::cout << scoreBoard[indexOfHS] << " " << indexOfHS << std::endl;
                traceback.push_back(Traceback::VERTICAL);
            }

            else// if (scoreBoard[indexOfHS] == scoreBoard[indexOfHS - 1] - gap)
            {
                indexOfHS--;
                //std::cout << scoreBoard[indexOfHS] << " " << indexOfHS << std::endl;
                traceback.push_back(Traceback::HORIZONTAL);
            }

        }
        indexOfHS = std::distance (scoreBoard.begin(), std::max_element(scoreBoard.begin(),scoreBoard.end()));
        itVert = indexOfHS/(seqHori_length+1)-1;
        itHori = indexOfHS%(seqHori_length+1)-1;

        didComputerun = true;
        return;
    }
}
/*int Alignment::getScoreCell (int k)
{
    return scoreBoard[k];
}*/


int Alignment::score() const
{
    if (didComputerun) return score_;
    else throw std::invalid_argument ("compute wasn't called");
}

void Alignment::getAlignment(std::string &a1, std::string &gaps, std::string &a2) const
{
    a1.clear();
    a2.clear();
    gaps.clear();
    if (didComputerun)
    {
        int iV = itVert;
        int iH = itHori;


        if (!localign)
        {
            for (Traceback trace: traceback)
            {
                if      (trace == Traceback::NONE || trace == Traceback::DIAGONAL)
                {
                    //std::cout << seqVert[iV] << seqHori[iH] << std::endl;
                    a1.push_back(seqVert[iV++]);
                    a2.push_back(seqHori[iH++]);


                    if (trace == Traceback::NONE)
                        gaps.push_back (' ');
                    else gaps.push_back('|');
                }
                else if (trace == Traceback::VERTICAL)
                {
                    //std::cout << seqVert[iV] << '-' << std::endl;
                    a1.push_back(seqVert[iV++]);
                    a2.push_back('-');

                    gaps.push_back(' ');
                }
                else
                {
                    //std::cout << '-' << seqHori[iH] << std::endl;
                    a1.push_back('-');
                    a2.push_back(seqHori[iH++]);

                    gaps.push_back(' ');
                }

            }
        }
        else
        {
            for (Traceback trace: traceback)
            {
                if      (trace == Traceback::NONE || trace == Traceback::DIAGONAL)
                {
                    a1.push_back(seqVert[iV--]);
                    a2.push_back(seqHori[iH--]);

                    if (trace == Traceback::NONE)
                        gaps.push_back(' ');
                    else
                        gaps.push_back('|');
                }
                else if (trace == Traceback::VERTICAL)
                {
                    a1.push_back(seqVert[iV--]);
                    a2.push_back('-');

                    gaps.push_back(' ');
                }
                else // if (trace == Traceback::HORIZONTAL)
                {
                    a1.push_back('-');
                    a2.push_back(seqHori[iH--]);

                    gaps.push_back(' ');
                }
            }
            std::reverse(a1.begin(), a1.end());
            std::reverse(a2.begin(), a2.end());
            std::reverse(gaps.begin(), gaps.end());
        }
        std::cout << a1 << std::endl
                  << gaps << std::endl
                  << a2 << std::endl;
    }
    else throw std::invalid_argument ("compute wasn't called");
}


