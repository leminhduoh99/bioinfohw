#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include "Alignment.hpp"

int main(int argc, const char* argv[])
{
    if (argc == 3 || argc == 6 || argc == 7) {

        Alignment workAl (argv[1], argv[2]);

        bool local_align = 0;
        int match = 3;
        int mismatch = -1;
        int gap =-2;
        std::stringstream argv3;
        argv3 << argv[3];
        std::stringstream argv4;
        argv4 << argv[4];
        std::stringstream argv5;
        argv5 << argv[5];

        if (argc == 6)
        {
            argv3 >> match;
            argv4 >> mismatch;
            argv5 >> gap;
        }
        std::string a1, gaps, a2;
        if (argc == 7) local_align = 1;
        if (match != NULL && mismatch != NULL && gap != NULL)
        {
            workAl.compute(match, mismatch, gap, local_align);
            workAl.getAlignment(a1, gaps, a2);
            std::cout << "score:" << workAl.score() << std::endl;
        }
        else
        {

            std::cerr << "Unexpected input" << std::endl;
            std::exit (EXIT_FAILURE);

        }
    }

    else
    {

        std::cerr << "Unexpected input" << std::endl;
        std::exit (EXIT_FAILURE);

    }

    return 0;
}