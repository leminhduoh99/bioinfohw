#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include "a4_util.h"
#include "BLAST_Neighborhood.hpp"
#include <omp.h>

int main(int argc, const char* argv[])
{
    if (argc == 6) {
        BLAST_Neighborhood workAl;
        int word_size;
        int score_threshold;
        int threads;
        ScoreMatrix matrix;
        std::string query = argv[1];

        
        matrix.load(argv[2]);
        

        std::stringstream argv3;
        argv3 << argv[3];
        argv3 >> word_size;
        
        std::stringstream argv4;
        argv4 << argv[4];
        argv4 >> score_threshold;

        std::stringstream argv5;
        argv5 << argv[5];        
        argv5 >> threads;
        
        double runtime = -omp_get_wtime();
        std::vector<NHResult> results = workAl.generateNeighborhood(query, matrix, word_size, score_threshold, threads);
        runtime += omp_get_wtime();

        for (unsigned i = 0; i < results.size(); ++i) {
            std::cout<<results[i].infix<<":";
            for (unsigned j = 0; j < results[i].neighbours.size(); ++j) {
                std::cout<<" ("<<results[i].neighbours[j].first<<", "<<results[i].neighbours[j].second<<")";
            }
            std::cout<<std::endl;
        }
        std::cout << "time: " << runtime <<"s." << std::endl;
    } else {
        std::cerr << "Unexpected input" << std::endl;
        std::exit (EXIT_FAILURE);
    }
    return 0;
}
