  
#include <math.h>
#include <string>
#include <vector>
#include <omp.h>
#include "BLAST_Neighborhood.hpp"
#include "a4_util.h"



/**
   Result data for a single infix
   e.g. infix: "AHI" 
         neighbours: (AHI, 16); (AHL, 14); (AHV, 15)
    Remember: members in structs are public by default.
              So you can initialize and use them like this (for example; other approaches possible):
      std::vector< std::pair <std::string, int> > my_vec;
      my_vec.push_back(make_pair("AGT", 23));
      my_vec.push_back(make_pair("ATT", 21));
      NHResult r{"AAA", my_vec};
      
      NHResult other_r;
      other_r.infix = "BLA";
      other_r.neighbours = my_vec;
 */


  //public:
    /**
     Berechnet die BLAST Nachbarschaft fuer eine Query.
    
     @param matrix          Die Scoring-Matrix fuer das Berechnen der Nachbarschaft
     @param word_size       Die zu verwendende Wortgroesse
     @param score_threshold Der minimale Score den ein Wort haben muss um zur Nachbarschaft zugehoeren
     @param threads         Die Anzahl der Threads die verwendet werden sollen
     @return Die Nachbarschaften der Query-Infixe, sortiert nach Position der Infixe in Query (d.h. 1. Infix, 2. Infix, ...).
	         Die Nachbarschaft jedes Infixes ist alphabetisch sortiert.
     @throw Wirft eine Exception, wenn @p threads != 1 ist (ohne Zusatzaufgabe), bzw <= 0 (mit Zusatzaufgabe)
    */
    std::vector<NHResult> BLAST_Neighborhood::generateNeighborhood(const std::string& query,
                                               const ScoreMatrix& matrix,
                                               const int word_size,
                                               const int score_threshold,
                                               const int threads)
    {
      
      if (threads < 1){
        throw std::invalid_argument( "received negative thread count" );
      }

      std::vector<NHResult> results;

      if (word_size > query.length()){
        return results;
      }  
      
      // generate infixes
      for (unsigned i = 0; i <= query.length() - word_size; ++i) {
        NHResult temp;
        temp.infix = query.substr(i, word_size);
        results.push_back(temp);
      }


      //Check every infix and every word

      const int base = TranslationTableAminoAcids::ALPHABET_SIZE; //Alphabet size
      //const size_t length = word_size;
      int size = pow(base,word_size); // 20^w quantity of word variations
      int m = results.size(); //quantity of infixes
      
      for (int i = 0; i < m; i += threads) { //current infix index
        #pragma omp parallel num_threads(threads)
        {
          int ID = omp_get_thread_num(); //thread ID = 0..threads

          //infixes will be checked in blocks of _threads_ count.
          //check out of bounds if the block size is larger than number of infixes to check
          if ((ID + i) < m) {

            std::string infix = results[ID + i].infix; //current infix
            
            //words generator
            for(int j = 0; j < size; ++j) { //current word variation 
              std::string word;

              for(int n = j; n > 0;) { //third parameter is n /= base;
                  int d = n%base;   //code of next char in word (from the end)
                  word = toAminoAcid(d) + word; //add char to word
                  n /= base; //go to next char
              }
              
              //add leading chars to begin of the word if generated word is shorter than word_size
              //for word_size=3 (C) => (AAC), word_size=4 (A) => (AAAA) 
              while(word.length() < word_size) word = toAminoAcid(0) + word; 
              
              int word_score = getScore(infix, word, matrix); //score for current word and infix
              
              if (word_score >= score_threshold ) {
                results[ID + i].neighbours.push_back(std::make_pair(word,word_score));
              }
            }
          }
        } //pragma block
      }  


      return results;
    }
//private:
    int BLAST_Neighborhood::getScore(const std::string& a, const std::string& b, const ScoreMatrix& matrix){
      if (a.length() != b.length()){
        throw std::invalid_argument( "words length is not equal");
      }
      int score = 0;
      for (unsigned i = 0; i < a.length(); ++i) {
        score += matrix.score(a[i], b[i]);
      }

      return score;
    }



   /*  bool sortfunction (std::pair <int, char> i, std::pair <int, char> j) { 
      return (i.first > j.first); 
    }


    std::vector<std::pair <int, char>> BLAST_Neighborhood::matrixsort(const ScoreMatrix& matrix) {
      
      const int base = TranslationTableAminoAcids::ALPHABET_SIZE;
      std::vector< std::pair <int, char>> msort;


      for (unsigned i = 0; i < base; ++i ) {
      char A = toAminoAcid(i);
        for (unsigned j = 0; j < base; ++j ) {
          char B = toAminoAcid(j);
          msort.push_back(std::make_pair(matrix.score(A, B), B));
        }
      }

      for(unsigned i = 0; i < base; ++i) 

      std::sort(msort.begin() + base * i, msort.begin() + base * (i+1), sortfunction);

      return msort;

    } */
