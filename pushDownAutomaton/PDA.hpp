#pragma once
#include <stack>
#include <map>
#include <utility>
#include <iostream>
#include <vector>
#include <string>

class PDA
{
public:
    
    enum class Language {
      HAIRPIN, ///< accepts RNA Hairpins
      BRACKETS ///< Zusatzaufgabe
    };

    enum class State {
      IN_PROGRESS = 0, ///< sequence is valid so far, but not complete
      SUCCESS = 1,     ///< currently in accepting state, i.e. stack is empty after reading final $ symbol
      FAIL = 2         ///< sequence is not in language
    };

    /// Constructor, which specifies which language to check (HAIRPIN by default)
    /// and internally builds up the production rules
    PDA(const Language l = Language::HAIRPIN);
    
    /// Read the next symbol and internally traverse states
    /// If a fail state was reached in the previous call, the fail state will be maintained
    /// @param a The symbol to read
    /// @return current state of PDA
    State next(const char a);
    
    /// Reset the automaton to a state as if newly constructed
    /// I.e. init stack and set state to s0 (IN_PROGRESS).
    void reset();

    
protected:
    /// YOUR Member variables and functions

    State state; //Current state
    std::stack<char> basement; //Stack (Keller)

    //Key-Part of the rule.
    struct Key
    {
      char input;       //Input char
      char stacktop;    //Current char in stack
      PDA::State state; //Current state

      //define < operator for using in map
      bool operator<(Key const& other) const {
        if (input < other.input) return true;
        if (input > other.input) return false;
        if (stacktop < other.stacktop) return true;
        if (stacktop > other.stacktop) return false;
        if (state < other.state) return true;
        return false;
      }
    };

    //Action-Part of the rule
    struct Action 
    {
      State new_state; //New state for automat
      std::string new_stack_symbols; //New symbols for stack
    };

    std::map<Key, Action> rules; //Set of rules

    //FUNKTIONS:
    //Get complement for char
    char complement (const char &c) {
      switch(c) {
        case 'a': return 'u';
        case 'c': return 'g';
        case 'g': return 'c';
        case 'u': return 'a';
        default : return ' ';
      }
    }

    //Add rule to automat (Input,Stacktop,State,NewState,AddString)
    void add_rule (const char &input, const char &stacktop, const State &state, 
                   const State &new_state, const std::string &str) 
    {
      Key k;
        k.input = input;
        k.stacktop = stacktop;
        k.state = state;

      Action a;
        a.new_state = new_state;
        a.new_stack_symbols = str;
        rules.insert({k, a});
    }
};
