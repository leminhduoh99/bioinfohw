#pragma once
#include <stack>
#include <map>
#include <utility> 
#include <iostream>
#include "PDA.hpp"

    /// Constructor, which specifies which language to check (HAIRPIN by default)
    /// and internally builds up the production rules
    PDA::PDA(const Language l) {
      reset(); //Initialize automat
      if (l == Language::HAIRPIN) {
        //generate rules for HAIRPIN automat
        for (auto c: {'a','c','g','u'}) {
          std::string s (1,'$');
          s += complement(c);
          s += '1';
          add_rule(c, '$', State::IN_PROGRESS, State::IN_PROGRESS, s);   
        }

        for (auto w: {'1', '2'}) {
          for (auto c: {'a','c','g','u'}) {
            std::string s (1,complement(c));
            s += char(w+1);
            add_rule(c, w, State::IN_PROGRESS, State::IN_PROGRESS, s); 
          }
        }
        
        add_rule('g', '3', State::IN_PROGRESS, State::IN_PROGRESS, "a4");
        
        for (auto c:{'a','c'}) {
          add_rule(c, '4', State::IN_PROGRESS, State::IN_PROGRESS, "a"); 
        }

        for (auto c: {'a','c','g','u'})  {
          add_rule(c, c, State::IN_PROGRESS, State::IN_PROGRESS, ""); 
        }

        add_rule('$', '$', State::IN_PROGRESS, State::SUCCESS, ""); 

      }
      if (l == Language::BRACKETS) {
        //generate rules for BRACKETS automat
        add_rule('(', '(', State::IN_PROGRESS, State::IN_PROGRESS, "((");
        add_rule(')', '(', State::IN_PROGRESS, State::IN_PROGRESS, "");
        add_rule('(', '$', State::IN_PROGRESS, State::IN_PROGRESS, "$(");
        add_rule(')', '$', State::IN_PROGRESS, State::FAIL, "");
        add_rule('$', '$', State::IN_PROGRESS, State::SUCCESS, "");
      }
    }
    
    /// Read the next symbol and internally traverse states
    /// If a fail state was reached in the previous call, the fail state will be maintained
    /// @param a The symbol to read
    /// @return current state of PDA
    PDA::State PDA::next(const char a){
         
      if (state == PDA::State::IN_PROGRESS) {
        //Generate key for searching in map
        Key search_key;
          search_key.input = a;
          search_key.stacktop = basement.top();
          search_key.state = state;

        //Search for rule
        auto it = rules.find(search_key);
        
        if (it != rules.end()) {
          //if rule found
          basement.pop();
          //push new symbols in stack
          for (auto c: it->second.new_stack_symbols) {
            basement.push(c);
          }
          //Change current state according the rule
          state = it->second.new_state;
        } else {
          //rule was not found
          state = State::FAIL;
        }
      }
      return state; 
    }
    
    /// Reset the automaton to a state as if newly constructed
    /// I.e. init stack and set state to s0 (IN_PROGRESS).
    void PDA::reset() {
      basement = std::stack<char>(); //clear stack
      basement.push('$'); //Push in stack end-symbol
      state = State::IN_PROGRESS; //reset current state
    }
