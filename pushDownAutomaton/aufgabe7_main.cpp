#include <string>
#include <vector>
#include <iostream>
#include <exception>
#include "PDA.hpp"
#include <omp.h>

std::string StateToStr(PDA::State &s)
    {
        switch (s) {
            case PDA::State::IN_PROGRESS: return "IN_PROGRESS";
            case PDA::State::FAIL:        return "FAIL"; 
            case PDA::State::SUCCESS:     return "ACCEPT";
            default :                     return "UNKNOWN";
        }
    }

int main(int argc, const char* argv[])
{
    if (argc == 2) {
        
        std::string query = argv[1];
        
        PDA::Language l = PDA::Language::HAIRPIN;

        //Check BRACKETS
        for (auto c: query) {
            if (c == ')' || c == '(') {
                l = PDA::Language::BRACKETS;
                break;
            }
        }

        //Generate automat
        PDA automat(l);

        //Launch automat
        for (auto c : query) {
            if (l == PDA::Language::BRACKETS && c != ')' && c != '(') continue;
            if ((automat.next(c)) != PDA::State::IN_PROGRESS) break;
        }

        //Check state
        PDA::State query_state = automat.next('$');

        //Return 0 if SUCCESS
        std::cout << StateToStr(query_state) << std::endl;
        return query_state == PDA::State::SUCCESS ? 0 : 1;

    } else {
        std::cerr << "Unexpected input" << std::endl;
        std::exit (EXIT_FAILURE);
    }

}