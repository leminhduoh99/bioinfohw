#pragma once

#include <string>
#include <vector>
#include <limits>
#include <array>
#include <map>

/// A needle (at position #i, as passed into ACTrie's constructor) was found in the haystack (query) at position 'p'
/// (all values are 0-based)
/// Do NOT change this structure!
struct Hit
{
  Hit() = default;
  Hit(size_t i, size_t p) : index(i), pos(p) {};
  size_t index;
  size_t pos;
  bool operator==(const Hit& rhs) const
  {
    return index==rhs.index && pos==rhs.pos;
  }
};



/// An index (into a vector) representing the location of a node
/// You do not have to use this class, but it might be useful...
/// It could also be extended to contain an edge label (character), if your design requires it...
class Index
{
public:
  Index() = default;
  Index(size_t index) : i_(index) {};
  /// is this Index actually valid?
  bool isNull() const { return i_ == std::numeric_limits<size_t>::max(); }
  /// what is the index? (might be invalid, i.e. .isNull() == true)
  size_t pos() const { return i_; }
  /// allows to set the index, using `i.pos() = 3;`
  size_t& pos() { return i_; }

private:
  size_t i_ = std::numeric_limits<size_t>::max();
};


class ACTrie
{
public:
  /// Constructor with a vector of needles (i.e. multiple patterns to be found in a hackstack)
  /// Needles may only contain letters from 'A' - 'Z'. All other characters lead to a std::exception being thrown.
  /// At least one needle is required. Passing an empty vector leads to throwing of std::logic_error.
  ACTrie(const std::vector<std::string>& needles);

  /// Set a haystack (query) where the needles (patterns) are to be searched
  /// This also resets the current trie-node to ROOT
  void setQuery(const std::string& haystack);

  /// Resume at the last position in the haystack until any of the patterns is found: fill all patterns found via 'hits' and return true.
  /// If the end of the haystack is reached without a match, return false.
  /// When this function is called multiple times, it resumes from the last position (state of the trie).
  bool next(std::vector<Hit>& hits);


private:

  unsigned position; //haystack position
  std::string haystack; //query
  std::vector<std::string> needles; //patterns

  struct ACNode
  {
    ACNode* suffix_link;
    ACNode* output_link;
    ACNode* parent_link;

    std::map<char, ACNode*> children;
    std::vector<Index> needle_indices;
    unsigned depth; 
  };

  ACNode* current_node;
  ACNode *root = add_node(nullptr, 0); //init root

  //Add new node in trie
  ACNode* add_node (ACNode* parentlink, unsigned depth) {

    ACNode* node = new ACNode();
    node -> suffix_link = nullptr;
    node -> output_link = nullptr;
    node -> parent_link = parentlink;
    node -> depth = depth;

    Index i;
    std::vector<Index> needle_indices;
    needle_indices.push_back(i);
    node -> needle_indices = needle_indices;

    return node;
  }

  //Add hits from node and hits from ouputlinks in vector hits
  void fill_hits (unsigned j, ACNode* node, std::vector<Hit> &hits) {

    //fill hits from endnode
    for (unsigned i = 0; i < node->needle_indices.size(); ++i){
      size_t index = node->needle_indices[i].pos();
      Hit hit(index, j - (node -> depth - 1));
      hits.push_back(hit);
      }
    //check outputslink
    while (node -> output_link != nullptr) {
      node = node -> output_link;
      for (unsigned i = 0; i < node->needle_indices.size(); ++i){
        size_t index = node->needle_indices[i].pos();
        Hit hit(index, j - (node -> depth - 1));
        hits.push_back(hit);
      }
    }

  }

  

};



