#pragma once

#include <string>
#include <vector>
#include <limits>
#include <array>
#include <iostream>
#include <queue>
#include <map>
#include "ACTrie.hpp"


/// Constructor with a vector of needles (i.e. multiple patterns to be found in a hackstack)
/// Needles may only contain letters from 'A' - 'Z'. All other characters lead to a std::exception being thrown.
/// At least one needle is required. Passing an empty vector leads to throwing of std::logic_error.
/* ACTrie::ACTrie(const std::vector<std::string> &needles) : haystack(*(std::string *)nullptr),
                                                          needles(needles) */
ACTrie::ACTrie(const std::vector<std::string> &needles)
{
  unsigned needles_size = needles.size();

  if (needles_size == 0)
    throw std::invalid_argument("Needle list is empty");

  for (auto const &needle : needles)
  {
    for (const char &c : needle)
    {
      if (c < 'A' || c > 'Z')
        throw std::invalid_argument("Needles may only contain letters from 'A' - 'Z!");
    }
  }
 

  /////// Trie-Constructor //////

  current_node = root; //begin trie-creation with root
  
  for (unsigned i = 0; i < needles_size; ++i) //for each pattern
  {
    for (const char &c : needles[i])
    {
      if (current_node->children.count(c) == 0) //if the character does't exist
      {
        current_node->children.insert({c, add_node(current_node, current_node->depth + 1)}); //create new node
        current_node = current_node->children[c]; //take a step in new node
      }
      else
      {
        current_node = current_node->children[c]; //else just take a step in this node
      }
    }
    //Mark end node
    if (current_node->needle_indices[0].isNull()) current_node->needle_indices[0] = i; 
    else current_node->needle_indices.push_back(Index(i)); 
    current_node = root;
  }

  /////// Add suffix and output links ///////

  root->suffix_link = root; //begin with root
  std::queue<ACNode *> q; // queue for BFS

  for (auto &child : root->children) // if depth = 1, then suffix-link is root
  {           
      child.second->suffix_link = root;
      q.push(child.second);  // add all children of root in queue 
  }

  while (q.size()) 
  {
    current_node = q.front(); //take node from queue
    q.pop(); //delete this node from queue

    for (auto &child: current_node->children)
    {
      ACNode *temp = current_node->suffix_link; // follow from the current node to its suffix link
      char c = child.first;

      while (temp->children.count(c) == 0 && temp != root) //if there is no character c among children
                                                           //and root is not reached
        temp = temp->suffix_link;                          // folow the suffix link

      if (temp->children.count(c))      // if there is character c among children
        child.second->suffix_link = temp->children[c]; // add suffix link from current node to this node
      else
        child.second->suffix_link = root; //else add suffix-link to the root

      q.push(child.second); // add current node to the end of queue
    }

    // add output-link
    if (!current_node->suffix_link->needle_indices[0].isNull()) // if the node, which follow up the suffix link 
                                                                // of current node, is end node,
      current_node->output_link = current_node->suffix_link; // set suffix link of current node as output link
    else
      current_node->output_link = current_node->suffix_link->output_link; // else set output link of node, 
  }                                           // which follow up the suffix link of current node, as output link
  position = 0; //ready to search 
  
}

/* Ha's ACtrie proposal. Uses conventional "indexing" instead of class "Index", the usage of which proves to be more efficient (works).

ACTrie::ACTrie(const std::vector <std::string> &needles)
{
    unsigned needles_size = needles.size();
    if (needles_size == 0)
        throw std::invalid_argument("Needle list is empty");
    for (auto const &needle : needles)
    {
        for (const char &c : needle)
        {
            if (c < 'A' || c > 'Z')
                throw std::invalid_argument("Needles may only contain letters from 'A' - 'Z!");
        }
    }

    size_t j;  //depth counter
    size_t needleCount = 0; //needle's index
    for (auto const needle: needles)
    {
        size_t m = needle.length();
        for (uint i = 0; i < m; ++i)
        {
            current = root;
            j = 0;
            //a loop for the existing nodes
            while (j < m && current -> children[needle[j] - 'A'] != nullptr)
            {
                current = current -> children[needle[j] - 'A'];
                j++;
            }
            //a loop for creating new nodes
            while (j < m)
            {
                ACNode* newNode = addNode(current, j);
                current -> children[needle[j]-'A'] = newNode;
                current = newNode;
                j++;
            }
        }
        //terminal
        current -> needle_indices.push_back(needleCount);
        //the next needle
        needleCount++;
    }
    
    //creating suffix_links
    root -> suffix_link = root;
    std::queue <ACNode*> queue;
    
    //starting the queue with depth = 0
    for (auto &child: root -> children)
    {
        if (child != nullptr)
        {
            queue.push(child);
            child -> suffix_link = root;
        }

    }

    while (queue.size() != 0)
    {
        current = queue.front();
        //after obtaining the node, we don't need it in the queue
        queue.pop();

        for (uint i = 0; i < 26; ++i)
        {
            //no need to analysis something, that's not there
            if (current->children[i] == nullptr)
                continue;
            else
            //default suffix_link
                current -> children[i] -> suffix_link = root;
            queue.push(current->children[i]);
            ACNode* suffix = current -> suffix_link;
            //for every suffix_link, we look for Label(K)
            while (suffix -> children[i] == nullptr && suffix != root)
                suffix = suffix -> suffix_link;
            //if found
            if (suffix -> children[i] != nullptr)
                current->children[i]->suffix_link = suffix->children[i];

        }
        //if the suffix node has any patterns already
        if (!current->suffix_link->needle_indices.empty())
            current->output_link = current -> suffix_link;
        else
        //inductively
            current->output_link = current -> suffix_link ->output_link;
    }
    position = 0;
}
*/

/// Set a haystack (query) where the needles (patterns) are to be searched
/// This also resets the current trie-node to ROOT
void ACTrie::setQuery(const std::string &haystack)
{
  this -> haystack = haystack;
  current_node = root;
  position = 0;
}

/// Resume at the last position in the haystack until any of the patterns is found: fill all patterns found via 'hits' and return true.
/// If the end of the haystack is reached without a match, return false.
/// When this function is called multiple times, it resumes from the last position (state of the trie).
bool ACTrie::next(std::vector<Hit> &hits)
{
  hits.clear();

  for (unsigned j = position; j < haystack.length(); ++j) { // begin with end position of last search
    char c = haystack[j];

    if (current_node -> children.count(c)) { //if the character exist in automata
      current_node = current_node -> children[c]; // take step in this node

      if (!current_node->needle_indices[0].isNull()) { //if end node
        
          fill_hits(j, current_node, hits); // add all hits in vector
          position = j+1; // change position for next search
          return 1; // stop search and report hits
      } else { // else check repetitive patterns through suffix links

        ACNode* temp = current_node-> suffix_link;
        
        while (temp != root) { // follow suffix links until root reached
          if (!temp -> needle_indices[0].isNull()) {
            fill_hits(j,temp, hits);
            position = j+1;
            return 1;
          }        
          temp = temp -> suffix_link;
        }
      }

    } else { //if the character don't exist in automata
      while (current_node != root && current_node -> children.count(c) == 0) {
        current_node = current_node -> suffix_link; // follow suffix links until root reached
      }                                             // or the character will be found
      if (current_node -> children.count(c)) j--; // if character is found next step should be took to this node
    }
  }
  return 0;
}
