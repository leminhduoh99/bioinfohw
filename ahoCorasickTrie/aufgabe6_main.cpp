#include <string>
#include <vector>
#include <iostream>
#include <exception>
#include "ACTrie.hpp"
#include <omp.h>


int main(int argc, const char* argv[])
{
    if (argc >= 3) {
        
        std::string query = argv[1];
        std::vector<std::string> needles;
        for (unsigned i = 2; i < argc; ++i) {
            needles.push_back(argv[i]);
        }
    
        ACTrie trie(needles);

        trie.setQuery(query);

        std::vector<Hit> hits;
    
        while (trie.next(hits)){

        for (auto &hit : hits){
                std::cout << "pos "<< hit.pos << ", " << needles[hit.index] << std::endl;
            }
        }



    } else {
        std::cerr << "Unexpected input" << std::endl;
        std::exit (EXIT_FAILURE);
    }



    return 0;
}