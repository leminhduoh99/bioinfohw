#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <math.h>
#include "a5_util.hpp"
#include "QGramIndex.hpp"

/**
   The Q-Gram-Index implementation using Counting Sort for SA Construction
   assuming DNA alphabet (|Sigma| = 4).
   
   The text may be up to 2^32 in length, i.e. uint32_t is used internally for suffix array and q-gram index.
   The maximal supported value for q is 13.
   
*/
//class QGramIndex 

//public:

    /**
     @brief Constructor: build up a q-gram index from @p text.

     Internally, @p text is stored as const reference (const std::string& ), 
     i.e. make sure the text does not go out of scope as long as the class object is used.
     
     The index is build immediately.
    
     @param text The sequence (genome, ...) to be indexed
     @param q Length of q-gram (from 1 to 13).
     @throws std::invalid_argument("Invalid q!"); if NOT: 1 <= q <= 13
    */
    QGramIndex::QGramIndex(const std::string& text, const uint8_t q)
      :text(text), q(q) {

      if (q < 1 || q > 13) throw std::invalid_argument("Invalid q!");

      uint32_t text_length = text.length();

      uint32_t dir_size = pow(alphabet_size, q) + 1;
      dir.resize(dir_size, 0);  //fill the array with 0


      //counting sort
      for (uint32_t i = 0; i <= text_length - q; ++i) { // 2. q-gramme zählen
        uint32_t j = hash(text.substr(i, q));
        dir[j] ++;
      }
    

      for (uint32_t j = 1; j < dir_size; ++j) { // 3. Kumulative Summe bilden
        dir[j] += dir[j - 1];
      }
    
      suftab.resize(text_length - q + 1);

      for (uint32_t i = 0; i <= text_length - q; ++i) { // 4. Vorkommen einsortieren
        uint32_t j;
        if (i == 0) {
          j = hash(text.substr(i, q));
         } else {
           j = hashNext(j,text[i+q-1]);
         }
          dir[j]--;
          suftab[dir[j]] = i;
          
        
      }
      

    }

    /**
      @brief Returns the text.
      
      @return the full text
    */
    const std::string& QGramIndex::getText() const{

      return text;
    }
    
    /**
      @brief Returns a vector of indices into text, where the q-gram with hash @p h occurs.
      
      The vector might be empty, if the q-gram does not exist, i.e. empty range within the suffix array.
      The hash value must be within [0...|SIGMA|^q), otherwise an exception is thrown.
      
      @note The indices are in reverse order of occurrence in the text, or might be even "unordered"
      when OpenMP multithreading is used.    
      
      @param h Hash value of the q-gram
      @return Vector of hits (indices into text)
      @throws std::invalid_argument("Invalid hash!"); if h is outside of valid hash values
    */
    std::vector<uint32_t> QGramIndex::getHits(const uint32_t h) const {
      
      if (h < 0 || h >= dir.size()-1) throw std::invalid_argument("Invalid hash!");

      std::vector<uint32_t> results;
      
      for (uint32_t i = dir[h]; i < dir[h+1]; ++i) results.push_back(suftab[i]);

      return results;
    }
    
    /**
      @brief Get the length of q-grams, i.e. 'q'.
    */
    uint8_t QGramIndex::getQ() const {

      return q;
    }
    
    /**
      @brief Compute a full hash for a given q-gram.
      
      @param qgram A q-gram; must have size 'q'
      @return hash value of @p qgram
      @throws std::invalid_argument("Invalid q-gram. Wrong length!"); if qgram.size() != q
      
    */
    uint32_t QGramIndex::hash(const std::string& qgram) const {

      if (qgram.size() != q) throw std::invalid_argument("Invalid q-gram. Wrong length!");

      uint32_t hashValue = 0;

      for(uint8_t i = 0; i <= q - 1; ++i) {
        hashValue <<= 2;
        hashValue |= ordValue(qgram[i]);
      }
      
      return hashValue;
    }
    
    /**
       @brief Returns the next rolling hash for given new character and previous hash (of previous q-gram).
       
       The first character going out-of-scope is removed internally by bitmasking (i.e. no need to specify it).
       
       @param prev_hash Previous hash value
       @param new_pos new character (last position of current q-gram)
       @return The updated hash value
    */
    uint32_t QGramIndex::hashNext(const uint32_t prev_hash, const char new_pos) const {
      uint32_t hashValue = prev_hash;

      hashValue <<= 2;
      hashValue |= ordValue(new_pos);

      unsigned mask = ~0 << 2*q; // for q=3: ...11111000000
      hashValue &= ~mask;

      return hashValue;  
    }          





