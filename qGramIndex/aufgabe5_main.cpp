#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <exception>
#include "a5_util.hpp"
#include "QGramIndex.hpp"
#include <omp.h>


// Definition of the member function ScoreMatrix::load.
std::string load(const char* fileName)
{
    std::ifstream genom_file(fileName); // Open ifstream for passed file.
    
    if (!genom_file.good())  // Check stream could be opened.        
        throw "Could not read file!";
    
    std::string buffer;  // buffer used to getline.
    std::string output;  // results
    
    // Parse remaining file.
    while(getline(genom_file, buffer))  // Reads line by line.
    {
        output += buffer;
    }
    
    if (!genom_file.eof())
        throw "Failed to load the text!";

    return output;
}

int main(int argc, const char* argv[])
{
    if (argc == 3) {
        
        std::string text = load(argv[1]);
        std::string query = argv[2];

        QGramIndex qgram(text, query.length());
        std::vector<uint32_t> hits = qgram.getHits(qgram.hash(query));

        //hits output
        std::cout << query << ": ";
        for (uint32_t i = 0; i < hits.size(); ++i) {
            std::cout << hits[i] << " ";
        }
        std::cout << std::endl;



    } else {
        std::cerr << "Unexpected input" << std::endl;
        std::exit (EXIT_FAILURE);
    }

    return 0;
}